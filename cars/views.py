from sqlite3 import IntegrityError
from django.shortcuts import render
from cars.scraper import get_data
from django.http import HttpResponse
from django.template import loader

# search = 'Tacoma'
# url = f'https://honolulu.craigslist.org/search/cta?query={search}&min_price=&max_price='

# def update_search(term):
#     search = term
    


def show_cars(request):
    if request.method == 'POST':
        search = request.POST['search']
        url = f'https://honolulu.craigslist.org/search/cta?query={search}&min_price=&max_price='
    else:
        url = 'https://honolulu.craigslist.org/search/cta?query=Tacoma&min_price=&max_price='
    cars = get_data(url)
    context = {"cars": cars}
    return render(request, "cars/list.html", context)

